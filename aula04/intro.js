console.log("Hello World!");
let variavel = 5;
console.log("A variavel " + variavel + " é do tipo " + typeof(variavel));

// operadores matematicos

let idade = 40;

if (idade == undefined){
    console.log("Idade não foi criado");
}else{
    console.log("Sua idade é: " + idade);
}

function mostrarIdade(){
    let idade = 40;
    if(idade < 41){
     console.log("jovem");
    }else{
        console.log("Não muito jovem");
    }
}

mostrarIdade();

function mostrarIdade(vlIdade){
    if(vlIdade < 42){
     console.log("jovem");
    }else{
        console.log("Não muito jovem");
    }
}

mostrarIdade(42);
mostrarIdade(41);